This repository contains tools we used in order to compare the results from
libcangjie with the ones from other platforms:

* Windows as the reference implementation for Cangjie 3
* TODO: something for Cangjie 5 (Google's implementation on Android?)

It is organized into directories, each one corresponding to a given platform.

For every platform, running the tools will eventually give 26 text files:
`a.txt`, `b.txt`, and so on until `z.txt`.

Each file contains the list of results for all the possible codes starting by
the letter in the file name. So for example, `a.txt` contains all the
characters corresponding to codes starting with the `a` key.

Inside a file, each line lists the results of a given code in 速成, in the
order they are suggested on that platform.

Our goals are:

*   if libcangjie provides the same results as the reference platforms, then
    they should be ordered identically;
*   if libcangjie provides more or less results than the reference platforms,
    then the results in common should always be at the start and be ordered
    identically;

Ideally libcangjie should have identical results to the reference platforms for
the first few pages of results. (9 characters per page)

## Windows

Windows does not seem to provide their Cangjie input method as a reusable
library.

As a result, we wrote a tool to instrument the input method system, sending
keyboard events to the focused application.

Compile the tool:

    > C:\Windows\Microsoft.NET\Framework\v4.0.30319\csc.exe windows/get.cs

We tried having get all the results for all possible codes, but that seemed
to completely overwhelm the input method system, slowing the machine until it
became completely unresponsive, forcing a hard reboot. (losing what had been
done in the process)

As a result, the tool takes the starting letter as an argument, and you must
run it for each starting letter:

    > windows/get.exe a

The tool will wait 3 seconds, during which you must switch to an open Notepad
window, as well as switch to the 速成 input method.

After those 3 seconds, the tool will send keyboard events through the input
method system, and you will see the Notepad window quickly getting filled with
the resulting character.

Once it finished, save the file as `windows/{letter}.txt`. For example if you
passed `a` as the argument to the tool, then name the file `windows/a.txt`.

**Note:** Any idea to make the above procedure easier is welcome. 🙂️

Due to boring technical details with the way the input method is implemented on
Windows, the result text files need to be cleaned up a bit before they can be
compared with files from other platforms:

    $ ./windows/clean-results.py

This will give you the resulting text files in the `windows/` directory.

## libcangjie

On a machine with Python 3 and pycangjie installed, simply run:

    $ ./libcangjie/get-all.py

This will give you the resulting text files in the `libcangjie/` directory.

## Check compatibility

This repository includes a tool to verify our compatibility with the reference
platforms.

For example, the following command will compare the results from Windows with
the results from libcangjie:

    $ ./compare windows libcangjie

This prints a "compatibility" result, for a certain definition of
"compatibility":

*   we look at the results in order, and consider we failed at the first result
    which doesn't match;
*   for every matching result we get points;
*   the first page is worth more point than the second, which is worth more
    points than the third, which is worth more points than the fourth, wich is
    worth more points than the fifth; all pages after that are worth the same
    amount of points as the fifth;

Hopefully this is not a terrible definition of "compatibility", as it favours
the first pages of results, which really are what most people care about. Being
wrong in the later pages is still counted as incompatibility, but it's not as
bad as being wrong at the start.

The compatibility target (threshold) can be passed as an argument:

    $ ./compare --threshold=90 windows libcangjie
