# Contributing

Thank you for considering contributing to this project. Free software can only
thrive thanks to people like you who take the time to help.

Following these guidelines helps to communicate that you respect the time of
the developers managing and developing this project. In return, they should
reciprocate that respect in addressing your issue, assessing changes, and
helping you finalize your contributions.

## Filing issues

Whenever you experience a problem with this software, please let us know so we
can make it better.

But first, take the time to search through the list of [existing issues] to see
whether it is already known.

Be sure to provide all the information you can, as that will really help us
fixing your issue as quickly as possible.

[existing issues]: https://gitlab.freedesktop.org/cangjie/platform-comparisons/issues

## Project structure

We have one directory per platform we want to compare. Ad of today those are:

* Windows, as the reference Cangjie 3 implementation;
* libcangjie, our implementation;

Each directory contains a tool to obtain the data for the corresponding
platform, as well as the Cangjie data obtained with it.

The root directory also contains a tool to compare 2 platforms and print a
compatibility report.

## Submitting changes

If you want to implement a new feature, make sure you first open an issue to
discuss it and ensure the feature is desired.

We accept changes through the usual merge request process.

### Commit history

Try to keep your branch split into atomic commits.

For example, if you made a first commit to implement a change, then a second
one to fix a problem in the first commit, squash them together.

If you need help doing that, please mention it in your merge request and we
will guide you towards using Git to achieve a nice and clean branch history. ☺

### Commit messages

Commit messages are extremely important. They tell the story of the project,
how it arrived to where it is now, why we took the decisions that made it the
way it is.

Chris Beams wrote [a wonderful post](https://chris.beams.io/posts/git-commit/)
that we highly recommend you read. The highlight of the post are 7 rules we ask
you to try and follow:

1.  Separate subject from body with a blank line
2.  Limit the subject line to 50 characters
3.  Capitalize the subject line
4.  Do not end the subject line with a period
5.  Use the imperative mood in the subject line
6.  Wrap the body at 72 characters
7.  Use the body to explain what and why vs. how

We further want to add our own rules:

*   if the commit is related to an issue, please mention the id of that issue
    in the commit message; for example, use something like `Fixes #123`.
