#!/usr/bin/python3

import pathlib

import cangjie


LETTERS = 'abcdefghijklmnopqrstuvwxyz'
RESULT_DIR = pathlib.Path(__file__).parent


for path in RESULT_DIR.glob('*.txt'):
    path.unlink()

cj = cangjie.Cangjie(cangjie.versions.CANGJIE3, cangjie.filters.BIG5 | cangjie.filters.HKSCS | cangjie.filters.PUNCTUATION)

for k1 in LETTERS:
    k1_chars = []

    for k2 in LETTERS:
        code = f'{k1}*{k2}'

        try:
            chars = ''.join([
                c.chchar
                for c in sorted(cj.get_characters(f'{k1}*{k2}'), key=lambda x: x.frequency, reverse=True)
            ])

        except cangjie.errors.CangjieNoCharsError:
            chars = ''

        chars += '\n'
        k1_chars.append(chars)

    (RESULT_DIR / f'{k1}.txt').write_text(''.join(k1_chars))
