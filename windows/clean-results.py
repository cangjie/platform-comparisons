#!/usr/bin/python3

import math
import pathlib


NUMBERS = '123456789'
RESULT_DIR = pathlib.Path(__file__).parent


def paginate_line(line):
    pages = []
    num_pages = math.ceil(len(line) / 9)

    for n in range(0, num_pages):
        start = n * 9
        pages.append(line[start:start + 9])

    return pages


def remove_repeat_pages(pages):
    filtered_pages = []

    for i, page in enumerate(pages):
        if filtered_pages and page == pages[0]:
            # This is a repeat
            break

        filtered_pages.append(page)

    return filtered_pages


def prune_last_page(pages):
    if not pages:
        return pages

    # Don't modify in-place
    pages = pages[:]

    last_page = pages.pop()
    new = list(last_page)

    for i, c in sorted(enumerate(last_page), reverse=True):
        if i > 0 and c == last_page[0]:
            new.pop()

        else:
            # previous iteration was the first repeat character
            break

    pages.append(''.join(new))

    return pages


def clean_line(line):
    if all([c in NUMBERS for c in line]):
        # No results at all for this code, usually when it ends in 'z'
        # Use a special marker here, to make this script idempotent
        return '--\n'

    if line == '--':
        return '--\n'

    if len(line) <= 1:
        return line + '\n'

    if line[1] == '1':
        # Only one result for this line, the rest is numbers and spaces
        return line[0] + '\n'

    # First, paginate the line, as that will help filtering it
    char_pages = paginate_line(line)

    # Next, eliminate pages which are just a repeat of a previous page. This
    # happens when the code had less pages than what the Windows tool tried.
    char_pages = remove_repeat_pages(char_pages)

    # Finally, remove duplicates inside the last page. This happens when the
    # last page has less than 9 results.
    char_pages = prune_last_page(char_pages)

    return ''.join(char_pages) + '\n'


def clean_file(path):
    with path.open('r') as f:
        for line in f:
            line = line.strip(' \n\ufeff')

            if not line:
                continue

            yield clean_line(line)


for path in sorted(RESULT_DIR.glob('*.txt')):
    tmp = pathlib.Path(f'{path}.tmp')
    tmp.write_text(''.join(clean_file(path)))
    tmp.replace(path)
