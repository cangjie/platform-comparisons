using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Threading;

namespace ConsoleApplication3 {
    static class Program {
        static void Input(char c) {
            INPUT[] input = KeyPress(c);
            SendInput((uint)input.Length, input, Marshal.SizeOf(typeof(INPUT)));
            Thread.Sleep(200);
        }
        static void Space() {
            Input(' ');
        }

        static void NewLine() {
            Input((char)0x0D);
        }

        static int Main(string[] args) {
            if (args.Length != 1) {
                Console.WriteLine("Please provide a letter as the only argument");
                return 1;
            }

            /* This is the first letter of the code. We can't just do all codes
             * from "aa" to "zz" because at some point the Windows input method
             * seems to be overwhelmed and nothing works any more. So we do all
             * codes from "Xa" to "Xz", with "X" chosen by the user of the tool.
             */
            char k1 = char.Parse(args[0].ToUpper());

            /* Wait a bit to let the user focus a notepad window */
            Thread.Sleep(3000);

            for (char k2 = 'A'; k2 <= 'Z'; k2++) {
                for (int page = 1; page <= 11; page++) {
                    for (char num = '1'; num <= '9'; num++) {
                        Input(k1);
                        Input(k2);

                        for (int j = 2; j <= page; j++) {
                            Space();
                        }

                        Input(num);
                    }
                }

                /* Yes: 2 new lines */
                NewLine();
                NewLine();
            }

            return 0;
        }

        static INPUT[] KeyPress(params char[] vks) {
            var r = new List<INPUT>();
            foreach (var vk in vks) {
                r.Add(new INPUT {
                    Type = 1,
                    Data = { Keyboard = new KEYBDINPUT { KeyCode = (ushort)vk } }
                });
                r.Add(new INPUT {
                    Type = 1,
                    Data = { Keyboard = new KEYBDINPUT { KeyCode = (ushort)vk, Flags = 2 } }
                });
            }
            return r.ToArray();
        }

        [DllImport("user32.dll", SetLastError = true)]
        static extern UInt32 SendInput(UInt32 numberOfInputs, INPUT[] inputs, Int32 sizeOfInputStructure);

        internal struct INPUT {
            public UInt32 Type;
            public MOUSEKEYBDHARDWAREINPUT Data;
        }

        [StructLayout(LayoutKind.Explicit)]
        internal struct MOUSEKEYBDHARDWAREINPUT {
            [FieldOffset(0)]
            public MOUSEINPUT Mouse;

            [FieldOffset(0)]
            public KEYBDINPUT Keyboard;

            [FieldOffset(0)]
            public HARDWAREINPUT Hardware;
        }

        internal struct HARDWAREINPUT {
            public UInt32 Msg;
            public UInt16 ParamL;
            public UInt16 ParamH;
        }

        internal struct KEYBDINPUT {
            public UInt16 KeyCode;
            public UInt16 Scan;
            public UInt32 Flags;
            public UInt32 Time;
            public IntPtr ExtraInfo;
        }

        internal struct MOUSEINPUT {
            public Int32 X;
            public Int32 Y;
            public UInt32 MouseData;
            public UInt32 Flags;
            public UInt32 Time;
            public IntPtr ExtraInfo;
        }
    }
}
